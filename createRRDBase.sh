#!/bin/bash

ts=`date +"%s"`
max=0
min=9999999999999999999999999
getLongestAndShortest() {
	previosValue=`head -1 data/failtimeFiltered.dat |  cut -d ":" -f 1`
	while IFS= read -r line;
	do
        	sec=`echo ${line} |  cut -d ":" -f 1`
	        subtraction=$((${sec} - ${previosValue}))
        	if [[ ${max} -lt ${subtraction} ]];
                	then max=${subtraction}
        	fi
	        if [[ ${min} -gt ${subtraction} && ${line} != `head -1 data/failtimeFiltered.dat` ]];then
			min=${subtraction} 
        	fi

        	previosValue=${sec}
	done < "data/failtimeFiltered.dat";

	echo "${max} maximum value"
	echo "${min} minimum value"
}

fillingGapsAndCreateDB() {

	val=`head -1 data/failtimeFiltered.dat | cut -d ":" -f 1`
	begin=$(( ${val} - 1 ))
	rrdtool create data/apcupsd.rrd \
	        --step ${min} \
	        --start ${begin}  \
	  DS:powerfail:GAUGE:${max}:0:U \
	  RRA:MAX:0.5:1:104000 \


	startValue=0
	while IFS= read -r line;
	do
		if [[ ${startValue} != 0 ]]; then
			endValue=`echo ${line} | cut -d ":" -f 1`
			for ((;${startValue} < ${endValue}; startValue=${startValue} + ${min} ));do
				rrdtool update data/apcupsd.rrd -t powerfail "${startValue}:0";
			done;
		fi
		rrdtool update data/apcupsd.rrd -t powerfail ${line};
		left=`echo ${line} |  cut -d ":" -f 1`
		startValue=$(( ${left} + `echo ${line} |  cut -d ':' -f 2` ))
	done < "data/failtimeFiltered.dat"
}

getLongestAndShortest
fillingGapsAndCreateDB
echo "Execution duraiton of $0: $[ `date +"%s"` - $ts ] seconds"
