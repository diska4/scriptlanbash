#!/bin/bash

ts=`date +"%s"`

bash filterData.sh
bash createRRDBase.sh
bash createGraph.sh

echo "Execution duration of entire script: $[ `date +"%s"` - $ts ] seconds"

