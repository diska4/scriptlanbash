#!/bin/bash
ts=`date +"%s"`
 
## Graph for last year
/usr/bin/rrdtool graph data/powerfails_1y.png \
-w 1080 -h 768 -a PNG \
--slope-mode \
--start 1520953961 --end 1548720121 \
--font DEFAULT:7: \
--title "Powerfails duration 1 year" \
--watermark "`date`" \
--vertical-label "failure (s)" \
--lower-limit 0 \
--x-grid WEEK:5:WEEK:1:MONTH:1:0:%D \
--alt-y-grid --rigid \
--lower-limit 0.5 -o \
DEF:powerfail=data/apcupsd.rrd:powerfail:MAX \
LINE1:powerfail#0000FF:"Powerfails (s)"

echo "Execution duration of $0: $[ `date +"%s"` - $ts ] seconds"
