#!/bin/bash
ts=`date +"%s"`
OUTPUT="data/failtimeFiltered.dat"
echo -n '' > $OUTPUT
dateRegEx='[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|1[0-9]|2[0-9]|3[0-1]) (0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9] '
failureRegEx="^${dateRegEx}\+0(2|3)00  Power failure."
backRegEx="^${dateRegEx}\+0(2|3)00  Power is back. UPS running on mains."

failtime=0
backtime=0

while IFS= read -r line;
do
    if [[ ${line} != *"Power"* ]]; then
        continue
    fi
    if [[ ${line} =~ $failureRegEx ]]; then
        if [[ ${failtime} != 0 ]]; then # if was no backtime
            echo "`date -d "${failtime}" +"%s"`:1" >> ${OUTPUT}
            #failtime=0
            #continue
        fi
        failtime=`echo ${line} | sed -e "s/\+0\(2\|3\)00 Power failure.//g"`
    fi
    if [[ ${line} =~ $backRegEx ]]
    then
        backtime=`echo ${line} | sed -e "s/\+0\(2\|3\)00 Power is back. UPS running on mains.//g"`
        if [[ ${failtime} == 0 ]]; then # if was no failtime 
            echo "${backtime} backtime without failtime"
            echo "`date -d "${backtime}" +"%s"`:1" >> ${OUTPUT}
            continue
        fi

        failEpoc=`date -d "${failtime}" +"%s"`
        backEpoc=`date -d "${backtime}" +"%s"`
        dif=$[ ${backEpoc} - ${failEpoc}]
        if (( $dif < 0 )); then
            echo "ERROR!"
            backtime=0
            failtime=0
            break
        fi
        if (( ${dif} < 1 )); then
            dif=1
        fi
        echo "`date -d "${failtime}" +"%s"`:${dif}" >> ${OUTPUT}
        backtime=0
        failtime=0

    fi
    # echo "failtime: $failtime, backtime: $backtime"
done < "data/apcupsd.events";
echo "Execution duration of $0: $[ `date +"%s"` - $ts ] seconds"

